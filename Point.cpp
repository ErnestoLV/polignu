#include <iostream>
#include "Point.h"

Point::Point()
{
     x = 0;
     y = 0;
}

Point::Point(int xtemp, int ytemp)
{
    x = xtemp;
    y = ytemp;
}

int Point::getX()
{
    return x;
}

void Point::setX(int xtemp)
{
    x = xtemp;
}

int Point::getY()
{
    return y;
}

void Point::setY(int ytemp)
{
    y = ytemp;
}
