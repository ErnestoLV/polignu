#ifndef POLI_H_INCLUDED
#define POLI_H_INCLUDED
#include <string>
#include "Point.h"

using namespace std;

class Poli{

private:
    int sides;
    int apot;
    int s;


    Point vertices[10];

public:

    Poli();
    Poli(int si, int ap, int S);

    int getSides();
    void setSides(int);

    int getApot();
    void setApot(int);

    int getS();
    void setS(int);

    void calculateS();
    int perimeter();
    int area();

};

#endif // POLI_H_INCLUDED
