#include <iostream>
#include "Poli.h"
#include "Point.h"


using namespace std;

int main()
{
    int ladost;
    int apott;

    Poli Polik;

    cout << "Ingresa la cantidad de lados" << endl;
    cin >> ladost;
    Polik.setSides(ladost);

    cout << "Ingresa el apotema" << endl;
    cin >> apott;
    Polik.setApot(apott);

    Polik.calculateS();
    cout << "El perimetro es: " << endl;
    cout << Polik.perimeter() << endl;
    cout << "El area es de: " << endl;
    cout << Polik.area() << endl;
}
