#ifndef POINT_H_INCLUDED
#define POINT_H_INCLUDED
#include <string>

using namespace std;

class Point{

private:
    int x;
    int y;

public:
    Point();
    Point(int, int);

    int getX();
    void setX(int);

    int getY();
    void setY(int);

};

#endif // POINT_H_INCLUDED
