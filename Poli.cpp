#include "Poli.h"
#include <string>
#include "math.h"

using namespace std;

Poli::Poli(){
    sides = 0;
    apot = 0;
    s = 0;
}

Poli::Poli(int si, int ap, int S){
    sides = si;
    ap = ap;
    s = S;
}

int Poli::getSides(){
    return sides;
}

void Poli::setSides(int si){
    sides = si;
}

int Poli::getApot(){
    return apot;
}

void Poli::setApot(int ap){
    apot = ap;
}

int Poli::getS(){
    return s;
}

void Poli::setS(int S){
    s = S;
}

void Poli::calculateS(){
    setS((getApot()*2)/sqrt(3));
}

int Poli::perimeter(){
    return (getS()*getSides());
}

int Poli::area(){
    return (getApot()*perimeter()/2);
}
